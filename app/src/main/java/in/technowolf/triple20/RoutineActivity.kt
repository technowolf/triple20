package `in`.technowolf.triple20

import `in`.technowolf.triple20.Utility.showToast
import android.os.Bundle
import android.os.CountDownTimer
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import com.mikhaellopez.circularprogressbar.CircularProgressBar


class RoutineActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_routine)
        val circularProgressBar: CircularProgressBar = findViewById(R.id.routine_progressbar)
        val animationDuration = 500
        circularProgressBar.color = ContextCompat.getColor(this, R.color.colorAccent)
        circularProgressBar.backgroundColor = ContextCompat.getColor(this, R.color.colorPrimaryDark)
        circularProgressBar.progressBarWidth = 12F //resources.getDimension(R.dimen.progressBarWidth)
        circularProgressBar.backgroundProgressBarWidth = 18F//resources.getDimension(R.dimen.backgroundProgressBarWidth)

        object : CountDownTimer(20000, 1000) {

            override fun onTick(millisUntilFinished: Long) {
                val progress = (21000 - millisUntilFinished) / 1000
                circularProgressBar.setProgressWithAnimation(progress.toFloat(), animationDuration)
            }

            override fun onFinish() {
                showToast(applicationContext, "finish", 1)
            }
        }.start()

    }
}
