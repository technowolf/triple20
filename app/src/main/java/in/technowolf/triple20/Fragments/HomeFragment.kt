package `in`.technowolf.triple20.Fragments

import `in`.technowolf.triple20.R
import `in`.technowolf.triple20.Receivers.RemainderReceiver
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button


class HomeFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val rootView = inflater.inflate(R.layout.fragment_home, container, false)
        val appCompatActivity = activity as AppCompatActivity
        val toolbar = appCompatActivity.findViewById(R.id.activity_main_toolbar) as Toolbar
        val remainderReceiver = RemainderReceiver()

        appCompatActivity.setSupportActionBar(toolbar)
        appCompatActivity.supportActionBar?.title = "Home"

        val mStartTimer: Button = rootView.findViewById(R.id.btn_start_timer)
        val mStopTimer: Button = rootView.findViewById(R.id.btn_stop_timer)

        mStartTimer.setOnClickListener {
            remainderReceiver.startRemainder(rootView.context)
        }

        mStopTimer.setOnClickListener {
            remainderReceiver.stopRemainder(rootView.context)
        }



        return rootView
    }

}
