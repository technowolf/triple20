package `in`.technowolf.triple20.Fragments

import `in`.technowolf.triple20.R
import `in`.technowolf.triple20.Utility.logError
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.preference.Preference
import android.support.v7.preference.PreferenceFragmentCompat
import android.support.v7.widget.Toolbar


class SettingsFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        val appCompatActivity = activity as AppCompatActivity
        val toolbar = appCompatActivity.findViewById(R.id.activity_main_toolbar) as Toolbar

        appCompatActivity.setSupportActionBar(toolbar)
        appCompatActivity.supportActionBar?.title = "Settings"
        setPreferencesFromResource(R.xml.preferences, rootKey)
    }

    override fun onPreferenceTreeClick(preference: Preference?): Boolean {
        val key = preference?.key
        when (key) {
            "master_switch" -> {
                logError("PREFS", "MASTER_SWITCH")
                return true
            }
            else -> return false
        }
    }
}
