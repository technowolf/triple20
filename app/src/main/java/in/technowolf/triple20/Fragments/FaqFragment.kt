package `in`.technowolf.triple20.Fragments

import `in`.technowolf.triple20.R
import `in`.technowolf.triple20.Utility.Tools
import `in`.technowolf.triple20.Utility.ViewAnimation
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.NestedScrollView
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton


class FaqFragment : Fragment() {
    private lateinit var mNestedScrollView: NestedScrollView
    private lateinit var mToggleText1: ImageButton
    private lateinit var mHideText1: Button
    private lateinit var mLinearLayoutExpandText1: View
    private lateinit var mToggleText2: ImageButton
    private lateinit var mHideText2: Button
    private lateinit var mLinearLayoutExpandText2: View
    private lateinit var mToggleText3: ImageButton
    private lateinit var mHideText3: Button
    private lateinit var mLinearLayoutExpandText3: View
    private lateinit var mToggleText4: ImageButton
    private lateinit var mHideText4: Button
    private lateinit var mLinearLayoutExpandText4: View


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_faq, container, false)
        val appCompatActivity = activity as AppCompatActivity
        val toolbar = appCompatActivity.findViewById(R.id.activity_main_toolbar) as Toolbar

        appCompatActivity.setSupportActionBar(toolbar)
        appCompatActivity.supportActionBar?.title = "FAQ"

        // nested scrollview
        mNestedScrollView = rootView.findViewById(R.id.nested_scroll_view) as NestedScrollView

        //FAQ Question 1
        mToggleText1 = rootView.findViewById(R.id.btn_toggle_text_question1) as ImageButton
        mHideText1 = rootView.findViewById(R.id.btn_hide_text_question1) as Button
        mLinearLayoutExpandText1 = rootView.findViewById(R.id.linearlayout_expand_text_question1) as View
        mLinearLayoutExpandText1.visibility = View.GONE

        mToggleText1.setOnClickListener { toggleSection1(mToggleText1) }
        mHideText1.setOnClickListener { toggleSection1(mToggleText1) }

        //FAQ Question 2
        mToggleText2 = rootView.findViewById(R.id.btn_toggle_text_question2) as ImageButton
        mHideText2 = rootView.findViewById(R.id.btn_hide_text_question2) as Button
        mLinearLayoutExpandText2 = rootView.findViewById(R.id.linearlayout_expand_text_question2) as View
        mLinearLayoutExpandText2.visibility = View.GONE

        mToggleText2.setOnClickListener { toggleSection2(mToggleText2) }
        mHideText2.setOnClickListener { toggleSection2(mToggleText2) }

        //FAQ Question 3
        mToggleText3 = rootView.findViewById(R.id.btn_toggle_text_question3) as ImageButton
        mHideText3 = rootView.findViewById(R.id.btn_hide_text_question3) as Button
        mLinearLayoutExpandText3 = rootView.findViewById(R.id.linearlayout_expand_text_question3) as View
        mLinearLayoutExpandText3.visibility = View.GONE

        mToggleText3.setOnClickListener { toggleSection3(mToggleText3) }
        mHideText3.setOnClickListener { toggleSection3(mToggleText3) }

        //FAQ Question 4
        mToggleText4 = rootView.findViewById(R.id.btn_toggle_text_question4) as ImageButton
        mHideText4 = rootView.findViewById(R.id.btn_hide_text_question4) as Button
        mLinearLayoutExpandText4 = rootView.findViewById(R.id.linearlayout_expand_text_question4) as View
        mLinearLayoutExpandText4.visibility = View.GONE

        mToggleText4.setOnClickListener { toggleSection4(mToggleText4) }
        mHideText4.setOnClickListener { toggleSection4(mToggleText4) }

        return rootView
    }

    private fun toggleSection1(view: View) {
        val show = toggleArrow(view)
        if (show) {
            ViewAnimation.expand(mLinearLayoutExpandText1, object : ViewAnimation.AnimListener {
                override fun onFinish() {
                    Tools.nestedScrollTo(mNestedScrollView, mLinearLayoutExpandText1)
                }
            })
        } else {
            ViewAnimation.collapse(mLinearLayoutExpandText1)
        }
    }

    private fun toggleSection2(view: View) {
        val show = toggleArrow(view)
        if (show) {
            ViewAnimation.expand(mLinearLayoutExpandText2, object : ViewAnimation.AnimListener {
                override fun onFinish() {
                    Tools.nestedScrollTo(mNestedScrollView, mLinearLayoutExpandText2)
                }
            })
        } else {
            ViewAnimation.collapse(mLinearLayoutExpandText2)
        }
    }

    private fun toggleSection3(view: View) {
        val show = toggleArrow(view)
        if (show) {
            ViewAnimation.expand(mLinearLayoutExpandText3, object : ViewAnimation.AnimListener {
                override fun onFinish() {
                    Tools.nestedScrollTo(mNestedScrollView, mLinearLayoutExpandText3)
                }
            })
        } else {
            ViewAnimation.collapse(mLinearLayoutExpandText3)
        }
    }

    private fun toggleSection4(view: View) {
        val show = toggleArrow(view)
        if (show) {
            ViewAnimation.expand(mLinearLayoutExpandText4, object : ViewAnimation.AnimListener {
                override fun onFinish() {
                    Tools.nestedScrollTo(mNestedScrollView, mLinearLayoutExpandText4)
                }
            })
        } else {
            ViewAnimation.collapse(mLinearLayoutExpandText4)
        }
    }

    fun toggleArrow(view: View): Boolean {
        if (view.rotation == 0f) {
            view.animate().setDuration(200).rotation(180f)
            return true
        } else {
            view.animate().setDuration(200).rotation(0f)
            return false
        }
    }

}
