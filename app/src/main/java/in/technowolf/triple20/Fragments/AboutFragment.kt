package `in`.technowolf.triple20.Fragments

import `in`.technowolf.triple20.R
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

class AboutFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_about, container, false)
        val appCompatActivity = activity as AppCompatActivity
        val toolbar = appCompatActivity.findViewById(R.id.activity_main_toolbar) as Toolbar

        appCompatActivity.setSupportActionBar(toolbar)
        appCompatActivity.supportActionBar?.title = "About"

        return rootView
    }

}
