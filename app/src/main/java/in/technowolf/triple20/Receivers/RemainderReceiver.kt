package `in`.technowolf.triple20.Receivers

import `in`.technowolf.triple20.R
import `in`.technowolf.triple20.RoutineActivity
import `in`.technowolf.triple20.Utility.logError
import android.app.AlarmManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Context.ALARM_SERVICE
import android.content.Intent
import android.os.Build
import android.os.PowerManager
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat


class RemainderReceiver : BroadcastReceiver() {
    private val channelId = "Remainder Notifications"
    private var notificationId = 0

    override fun onReceive(context: Context, intent: Intent) {
        //TODO: Cleanup the onRecieve with a method call

        // This method is called when the BroadcastReceiver is receiving an Intent broadcast.
        val pm: PowerManager = context.getSystemService(Context.POWER_SERVICE) as PowerManager
        val wakelock: PowerManager.WakeLock =
            pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "WAKELOCK: Triple20 Remainder")


        wakelock.acquire()

//        try {
//            val notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
//            val r = RingtoneManager.getRingtone(context.applicationContext, notification)
//            r.play()
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
        notificationBuilder(context)
        wakelock.release()

    }

    fun startRemainder(context: Context) {
        logError("NOTIFICATION", "Started alarm service")
        val am = context.getSystemService(ALARM_SERVICE) as AlarmManager
        val intent = Intent(context, RemainderReceiver::class.java)

        val pendingIntent = PendingIntent.getBroadcast(
            context.applicationContext, 0, intent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )
        am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 1 * 60 * 1000L, pendingIntent)
    }

    fun stopRemainder(context: Context) {
        logError("NOTIFICATION", "Stopped alarm service")
        val intent = Intent(context, RemainderReceiver::class.java)
        val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val pendingIntent = PendingIntent.getBroadcast(
            context.applicationContext, 0, intent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )

        alarmManager.cancel(pendingIntent)
        pendingIntent.cancel()
    }

    private fun notificationBuilder(context: Context) {
        //TODO: add a notification builder

        val intent = Intent(context, RoutineActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        val routinePendingIntent: PendingIntent = PendingIntent.getActivity(context, 0, intent, 0)

        val mBuilder = NotificationCompat.Builder(context, channelId)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentTitle("Triple20 Routine")
            .setContentText("Its time to follow the routine.\n Tap to start")
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            //alarm category so it can alert user even in DND mode
            .setCategory(NotificationCompat.CATEGORY_ALARM)
            //setting activity launch on notification click
            .setContentIntent(routinePendingIntent)
            .setAutoCancel(true)

        with(NotificationManagerCompat.from(context)) {
            // notificationId is a unique int for each notification that you must define
            notify(notificationId, mBuilder.build())
        }
        notificationId++
    }

    fun createNotificationChannel(context: Context) {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "Remainder Notification"
            val descriptionText = "Reminds you every 20 minuets to follow routine in order to prevent eye strain"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(channelId, name, importance).apply {
                description = descriptionText
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }
}
