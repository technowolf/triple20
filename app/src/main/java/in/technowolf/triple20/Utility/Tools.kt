package `in`.technowolf.triple20.Utility

import android.support.v4.widget.NestedScrollView
import android.view.View

object Tools {

    fun nestedScrollTo(nested: NestedScrollView, targetView: View) {
        nested.post { nested.scrollTo(500, targetView.bottom) }
    }

}
