package `in`.technowolf.triple20

import `in`.technowolf.triple20.Fragments.AboutFragment
import `in`.technowolf.triple20.Fragments.FaqFragment
import `in`.technowolf.triple20.Fragments.HomeFragment
import `in`.technowolf.triple20.Fragments.SettingsFragment
import `in`.technowolf.triple20.Receivers.RemainderReceiver
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    //Initialized fragments beforehand so the UI will retain
    private var homeFragment = HomeFragment()
    private var settingsFragment = FaqFragment()
    private var aboutFragment = AboutFragment()
    private var preferencesFragment = SettingsFragment()
    private var remainderReceiver = RemainderReceiver()

    //Bottom navigation listener
    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                loadFragment(homeFragment)
                return@OnNavigationItemSelectedListener true
            }

            R.id.navigation_settings -> {
                loadFragment(preferencesFragment)
                return@OnNavigationItemSelectedListener true
            }

            R.id.navigation_faq -> {
                loadFragment(settingsFragment)
                return@OnNavigationItemSelectedListener true
            }

            R.id.navigation_about -> {
                loadFragment(aboutFragment)
                return@OnNavigationItemSelectedListener true
            }

        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        /* Loading home fragment whenever MainActivity is created */
        loadFragment(homeFragment)
        remainderReceiver.createNotificationChannel(applicationContext)

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }

    /*
    * use this method to replace fragment
    *
    * @param fragment holds Instance of Fragment
    * @return nothing
    * */
    private fun loadFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.activity_fragment, fragment)
            .commit()
    }

}
